const countSpan = document.getElementById('count')
const countbutton =document.getElementById('countButton')

let count = 0
countButton.addEventListener("click", incrementCount)

function incrementCount() {
   count = count + 1
   console.log(count)
   countSpan.innerHTML = count
}
